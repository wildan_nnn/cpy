#!usr/bin/python
import socket # mengimport modul socket agar dapat menggunakan library yang digunakan untuk komunikasi client server

s = socket.socket() # untuk membuat koneksi
host = ''           # menentukan host yang aktif atau bisa juga menggunakan function socket.gethostname()
port = 12345        # menentukan port yang digunakan untuk berkomunikasi
s.bind((host, port))# set supaya host dan port tersebut aktif

s.listen(5)         # mode yang digunakan untuk menunggu permintaan dari client
while True:         # kondisi apabila sudah menerima permintaan dari client
 c, addr = s.accept() # menerima permintaan dari client
 print ('Mendapatkan permintaan koneksi dari', addr) # cetak pesan yang menunjukkan alamat IP dari client
 c.send('Terima kasih atas permintaan koneksinya') # kirim pesan ke client
 c.close() # menutup koneksi
