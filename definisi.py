# Pendeklarasian class yang normal
class jkl:
    pass

ki = jkl()
print("{} ini".format(ki))

class Nama(object):
    """docstring for ."""
    def __init__(self, arg):
        self.arg = arg

dew = Nama("Wildan")
print("Nama saya {}".format(dew.arg))


# Pendeklarasian class menggunakan atribut instance
class Dog:
    spesies = "mammal"
    # Initializer / Instance Attributes
    def __init__(self, name, age):
        self.name = name
        self.age = age

rt = Dog("Rout",5)
jake = Dog("Jake",8)
down = Dog("Dow",6)

if (rt.spesies == 'mammal'):
    print("{0} itu {1}".format(rt.name,rt.spesies))
print("")
print("{} itu {}".format(rt.name,rt.age))
print("{} itu {}".format(jake.name,jake.age))
print("{} itu {}".format(down.name,down.age))

def get_nomor_terbesar(*args):
    return max(args)

# output
print ("The oldest Dog is {} years old.".format(
    get_nomor_terbesar(rt.age,jake.age,down.age)
))
