#Kondisi if adalah kondisi yang akan dieksekusi oleh program jika bernilai benar atau TRUE
import tkinter

nilai = 1

#jika kondisi benar/TRUE maka program akan mengeksekusi perintah dibawahnya
if(nilai < 7):
    print("Maaf Anda Tidak Lulus")

#jika kondisi salah/FALSE maka program tidak akan mengeksekusi perintah dibawahnya
if(nilai > 10):
    print("Selamat Anda Lulus")
